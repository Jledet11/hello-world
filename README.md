# Hello World

 “Tutorial for learning Git”
 
 Liar’s Dice Digital - A digital version of the tabletop game Liar’s Dice. It will utilize online multiplayer to 
 allow users to play against their friends. If you have no friends, you can play against bots.

 This project will be completed in Unity and will be used on mobile devices.

 author - Jansen Ledet (C00077268)